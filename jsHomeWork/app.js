var interval = setInterval(setColor, 1000);

function setColor(){
    var text = document.getElementById("text");
    text.style.color = text.style.color === 'red' ? 'black' : 'red';
}

var background = document.getElementById('backColor');

background.addEventListener('input', () => {
    var bColor = background.value;
    document.body.style.backgroundColor = bColor;
});

var textColor = document.getElementById('textColor');

textColor.addEventListener('input', () => {
    var tColor = textColor.value;
    document.body.style.color = tColor;
});