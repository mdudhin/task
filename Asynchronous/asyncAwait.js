const request = require('request');
const option = {
  url: 'https://api.github.com/repos/request/request',
  headers: {
    'User-Agent': 'request',
  },
};

let data = new Promise(function (resolve, reject) {
  return request(option, (error, response, body) => {
    if (!error && response.statusCode == 200) {
      const info = JSON.parse(body);
      resolve(info);
    }
    reject(new Error('Data not response'));
  });
});

async function main(){
    try {
        const result = await data
        console.log(result.name);
        console.log(result.stargazers_count + ' Stars');
        console.log(result.forks_count + ' Forks');
    } catch(err) {
        console.log(err.message);
    }
}

main()