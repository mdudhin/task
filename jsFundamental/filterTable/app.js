let xmlHttp = new XMLHttpRequest();
let url = 'planet.json';

xmlHttp.onreadystatechange = function () {
	if (this.readyState === 4 && this.status == 200) {
		let myArr = JSON.parse(this.responseText);
		myFunction(myArr);
	}
};

xmlHttp.open('GET', url, true);
xmlHttp.send();

function myFunction(arr) {
	let out = '';
	let i;
	let number;
	for (i = 0; i < arr.length; i++) {
		number = i + 1;
		out +=
			'<tr class="body"><th scope="row">' +
			number +
			'</th>' +
			'<td>' +
			arr[i].name +
			'</td>' +
			'<td>' +
			arr[i].diameter +
			'</td>' +
			'<td>' +
			arr[i].climate +
			'</td>' +
			'<td>' +
			arr[i].gravity +
			'</td>' +
			'<td>' +
			arr[i].population +
			'</td></tr>';
	}
	document.getElementById('table').innerHTML = out;
}

function search() {
	let input = document.getElementById('search');
	let filter = input.value.toUpperCase();
	let table = document.getElementById('planetTable');
	let tr = table.getElementsByTagName('tr');

	if (!!filter) {
		for (let i = 0; i < tr.length; i++) {
			let td = tr[i].getElementsByTagName('td')[0];

			if (td) {
				let txtValue = td.textContent || td.innerText;
				if (txtValue.toUpperCase().indexOf(filter) !== -1) {
					tr[i].style.color = 'red';
				} else {
					tr[i].style.color = 'black';
				}
			}
		}
	} else {
		alert('Kata pencarian tidak boleh kosong !');
	}
}
