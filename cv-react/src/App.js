import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import './responsive.css';
import './assets/txtRotate.js';

import {Container, Nav, Navbar, Carousel, Row, Col, Card, ProgressBar, Form, Button} from 'react-bootstrap';
import img1 from './assets/image/mdudhin.jpg';
import img2 from './assets/image/6.jpg';
import img3 from './assets/image/photo.png';

class App extends Component {
  render() {
    const now = [80, 70, 65, 75];
    const progressHtml = <ProgressBar now={now[0]} label={`${now[0]}%`} />;
    const progressCss = <ProgressBar now={now[1]} label={`${now[1]}%`} />;
    const progressKotlin = <ProgressBar now={now[2]} label={`${now[2]}%`} />;
    const progressVue = <ProgressBar now={now[3]} label={`${now[3]}%`} />;
    return (
      <div className="App" id="App">
        <header>
          <Container>
            <Navbar expand="lg" variant="dark">
              <Navbar.Brand className="logo">Mdudhin</Navbar.Brand>
              <Navbar.Toggle aria-controls="basic-navbar-nav" />
              <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="ml-auto nav-item">
                  <Nav.Link href="#App">Home</Nav.Link>
                  <Nav.Link href="#about">About</Nav.Link>
                  <Nav.Link href="#skill">Skill</Nav.Link>
                  <Nav.Link href="#contact" >Contact</Nav.Link>
                </Nav>
              </Navbar.Collapse>
            </Navbar>
          </Container>
        </header>

        <section>
          <Carousel>
            <Carousel.Item interval="100000">
              <img
                className="d-block w-100"
                src={img1}
                alt="First slide"
              />
              <Carousel.Caption>
                <span>Hello! My Name</span>
                <h1>Muhammad Dhiya Udhin</h1>
                <h2>
                  I'm a
                  <span class="txt-rotate" data-period="2000"
                    data-rotate='[ " Web Designer.", " Programmer.", " Mobile Developer." ]'></span>
                </h2>
              </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item interval="50000">
              <img
                className="d-block w-100"
                src={img2}
                alt="First slide"
              />
            </Carousel.Item>
          </Carousel>
        </section>

        <section id="about">
          <Container>
            <h3 className="section-title">About</h3>
            <Row>
              <Col md={6} className="image-profile">
                <img
                  src={img3}
                  alt="Profile"
                />
              </Col>
              <Col md={6} className="data">
                <Row>
                  <Col xs={12} className="personal-data">
                    <span>Name</span>
                    <p>Muhammad Dhiya Udhin</p>
                  </Col>
                  <Col xs={12} className="personal-data">
                    <span>Age</span>
                    <p>23 Years Old</p>
                  </Col>
                  <Col xs={12} className="personal-data">
                    <span>Address</span>
                    <p>Palembang, Indonesia</p>
                  </Col>
                  <Col xs={12} className="personal-data">
                    <span>Email</span>
                    <p>muhammaddhiya.udhin@gmail.com</p>
                  </Col>
                  <Col xs={12} className="personal-data">
                    <span>Phone</span>
                    <p>+62-813-6743-0773</p>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Container>
        </section>

        <section id="skill">
          <Container>
            <h3 className="section-title">Skills</h3>
            <Row className="skills">
              <Col md={6} className="skill text-center">
                <Card>
                  <Card.Header><i class="fab fa-html5"></i></Card.Header>
                  <Card.Body>
                    {progressHtml}
                  </Card.Body>
                  <Card.Footer><p>HTML</p></Card.Footer>
                </Card>
              </Col>
              <Col md={6} className="skill text-center">
                <Card>
                  <Card.Header><i class="fab fa-html5"></i></Card.Header>
                  <Card.Body>
                    {progressCss}
                  </Card.Body>
                  <Card.Footer><p>CSS</p></Card.Footer>
                </Card>
              </Col>
              <Col md={6} className="skill text-center">
                <Card>
                  <Card.Header><i class="fab fa-html5"></i></Card.Header>
                  <Card.Body>
                    {progressKotlin}
                  </Card.Body>
                  <Card.Footer><p>Kotlin</p></Card.Footer>
                </Card>
              </Col>
              <Col md={6} className="skill text-center">
                <Card>
                  <Card.Header><i class="fab fa-html5"></i></Card.Header>
                  <Card.Body>
                    {progressVue}
                  </Card.Body>
                  <Card.Footer><p>VUE</p></Card.Footer>
                </Card>
              </Col>
            </Row>
          </Container>
        </section>

        <section id="contact">
          <Container>
            <h3 className="section-title">Contact</h3>
            <Form>
              <Form.Group controlId="name">
                <Form.Label>Name</Form.Label>
                <Form.Control type="text" placeholder="Enter name" />
              </Form.Group>

              <Form.Group controlId="email">
                <Form.Label>Email address</Form.Label>
                <Form.Control type="email" placeholder="Enter email" />
              </Form.Group>

              <Form.Group controlId="subject">
                <Form.Label>Subject</Form.Label>
                <Form.Control type="text" placeholder="Enter subject" />
              </Form.Group>

              <Form.Group controlId="messages">
                <Form.Label>Messages</Form.Label>
                <Form.Control as="textarea" rows="3" placeholder="Enter messages" />
              </Form.Group>

              <Form.Group>
                <Button variant="primary" type="submit">
                  Submit
                </Button>
              </Form.Group>
            </Form>
          </Container>
        </section>

        <footer>
          <Container>
            <Row>
              <Col></Col>
              <Col></Col>
              <Col className="social-media">
                <a href="https://www.instagram.com/mdudhin/"><i class="fab fa-instagram"></i></a>
              </Col>
              <Col className="social-media">
                <a href="https://www.facebook.com/muhammad.udhin"><i class="fab fa-facebook-square"></i></a>
              </Col>
              <Col className="social-media">
                <a href="https://twitter.com/Udhin_SAO"><i class="fab fa-twitter"></i></a>
              </Col>
              <Col></Col>
              <Col></Col>
            </Row>
          </Container>
        </footer>
      </div>
    );
  }
}

export default App;
