class Student {
    constructor(name, age, dateOfBirth, gender, studentId){
        this.name = name;
        this.age = age;
        this.dateOfBirth = dateOfBirth;
        this.gender = gender;
        this.studentId = studentId;
        this.hobbies = ['Gaming', 'Hunting'];
    }

    get data() {
        return this
    }

    set setName(value){
        this.name = value
    }

    set setAge(value){
        this.age = value
    }

    set setDateOfBirth(value){
        this.dateOfBirth = value
    }

    set setGender(value){
        if(value == 'Male' || value == 'Female'){
            this.gender = value
        } else {
            console.log('Format gender must be Male or Female!')
        }
    }

    addHobby(value){
        this.hobbies.push(value)
    }

    deleteHobby(value){
        let position = this.hobbies.indexOf(value)
        this.hobbies.splice(position, 1)
    }
}

const student = new Student('Cha Hae-in', 23, '19 Mei 1997', 'Female', 'SID00142');

//console.log(student.data);
student.setName = 'Sung Jin-Woo';
student.setAge = 25 ;
student.setDateOfBirth = '22 April 1995';
student.setGender = 'Male';
student.addHobby('Cooking')
student.deleteHobby('Hunting')
console.log(student.data)